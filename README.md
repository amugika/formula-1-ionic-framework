****************************************************************************
EUSKARA
****************************************************************************

Ionic proiektuak garatzeko NodeJS, Cordova eta Ionic behar-beharrezkoak dira.

* NodeJS: https://nodejs.org

* npm install -g ionic cordova

* Ionic azken bertsioa: 1.6.1
* Cordova azken bertsioa: 5.1.1

## Beharrezkoa den dokumentazioa ##
* Ionic dokumentazioa: http://ionicframework.com/docs/
* AngularJS: 

## Instalaturik dauden plugin zerrenda: ##

**White List:** cordova plugin add https://github.com/apache/cordova-plugin-whitelist

****************************************************************************
ESPAÑOL
****************************************************************************

Para desarrollar proyectos en framework Ionic necesitaremos NodeJS, Cordova e Ionic.

* NodeJS: https://nodejs.org

* npm install -g ionic cordova

* Ionic (última versión): 1.6.1 (No es de las librerías, es del CLI)
* Cordova (última version): 5.1.1

## Documentación necesaria para el desarrollo ##
* Ionic: http://ionicframework.com/docs/
* AngularJS: 

## Lista de plugins instalados: ##

**White List:** cordova plugin add https://github.com/apache/cordova-plugin-whitelist