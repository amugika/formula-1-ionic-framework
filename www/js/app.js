// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('f1App', 
          ['ionic',
            'f1App.About',
            'f1App.controllers',
            'f1App.Season',
            'f1App.Season',
            'f1App.Circuits',
            'f1App.Circuit',
            'f1App.Drivers',
            'f1App.Driver',
            'f1App.Main'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {

    //http://codepen.io/Rai/pen/rVjzoX example material
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: "/search",
    views: {
      'menuContent': {
        templateUrl: "templates/search.html",
        controller: 'SearchCtrl'
      }
    }
  })

  .state('app.browse', {
    url: "/browse",
    views: {
      'menuContent': {
        templateUrl: "templates/browse.html"
      }
    }
  })

  .state('app.drivers', {
    url: "/drivers",
    views: {
      'menuContent': {
        templateUrl: "templates/drivers/all.html",
        controller: "DriversCtrl"
      }
    }
  })

   .state('app.driver', {
    url: "/driver/:id/:year",
    views: {
      'menuContent': {
        templateUrl: "templates/drivers/driver.html",
        controller: 'DriverCtrl'
      }
    }
  })

  .state('app.circuits', {
    url: "/circuits",
    views: {
      'menuContent': {
        templateUrl: "templates/circuits/all.html",
        controller: "CircuitsCtrl"
      }
    }
  })
    .state('app.main', {
      url: "/main",
      views: {
        'menuContent': {
          templateUrl: "templates/main.html",
          controller: 'MainCtrl'
        }
      }
    })
    .state('app.about', {
      url: "/about",
      views: {
        'menuContent': {
          templateUrl: "templates/about.html",
          controller: 'AboutCtrl'
        }
      }
    })

    .state('app.circuit', {
    url: "/circuit/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/circuits/circuit.html",
        controller: 'CircuitCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/main');
});
