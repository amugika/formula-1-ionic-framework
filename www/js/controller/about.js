/**
 * @ngdoc function
 * @name f1App.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the f1App
 */
angular.module('f1App.About', [])
  .controller('AboutCtrl', function ($scope) {
    $scope.about='about';
  });


